  CREATE TABLE public."user"
(
  id bigint NOT NULL,
  name text,
  CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.account
(
  id bigint NOT NULL,
  amount bigint,
  currency character(3),
  user_id bigint,
  CONSTRAINT account_pkey PRIMARY KEY (id),
  CONSTRAINT account_foreign_key FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO public."user" VALUES (1,'user');
INSERT INTO public."user" VALUES (2,'user2');
INSERT INTO public."user" VALUES (3,'user3');
INSERT INTO public.account VALUES (1,0,'USD', 1);
INSERT INTO public.account VALUES (2,0,'EUR', 1);
INSERT INTO public.account VALUES (3,0,'GBP', 1);
INSERT INTO public.account VALUES (4,0,'USD', 2);
INSERT INTO public.account VALUES (5,0,'EUR', 2);
INSERT INTO public.account VALUES (6,0,'GBP', 2);
INSERT INTO public.account VALUES (7,0,'USD', 3);
INSERT INTO public.account VALUES (8,0,'EUR', 3);
INSERT INTO public.account VALUES (9,0,'GBP', 3);
