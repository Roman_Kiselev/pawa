package ee.pawa.action;

public enum ActionType {
    DEPOSIT, WITHDRAW, BALANCE
}
