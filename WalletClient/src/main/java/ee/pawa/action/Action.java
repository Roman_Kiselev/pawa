package ee.pawa.action;

public class Action {
    private final ActionType type;
    private final String amount;
    private final Currency currency;

    public Action(ActionType type, String amount, Currency currency) {
        this.type = type;
        this.amount = amount;
        this.currency = currency;
    }

    public ActionType getType() {
        return type;
    }

    public String getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
