package ee.pawa.management;

import ee.pawa.action.Action;
import ee.pawa.grpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundExecutor {
    private final WalletServiceGrpc.WalletServiceBlockingStub stub;
    private final Round round;
    private final long userId;

    private static final Logger log = LoggerFactory.getLogger(RoundExecutor.class);

    public RoundExecutor(WalletServiceGrpc.WalletServiceBlockingStub stub, Round round, long userId) {
        this.stub = stub;
        this.round = round;
        this.userId = userId;
    }

    public void execute() {
        Action action;
        while ((action = round.getAction()) != null) {
            switch (action.getType()) {
                case BALANCE: {
                    BalanceResponse balanceResponse = stub.balance(
                            BalanceRequest.newBuilder().setUserId(userId).build());
                    log.debug("user {} getBalance:", userId);
                    balanceResponse.getBalanceList()
                            .forEach(balance -> log.debug("account currency: {} amount: {}", balance.getCurrency(),
                                    balance.getAmount()));
                    break;
                }
                case DEPOSIT: {
                    Response deposit = stub.deposit(
                            DepositRequest.newBuilder()
                                    .setCurrency(action.getCurrency().name())
                                    .setAmount(action.getAmount())
                                    .setUserId(userId)
                                    .build());
                    log.debug("user {} has made deposit {}  {}  result {}", userId, action.getAmount(),
                            action.getCurrency(), deposit.getMessage());
                    break;
                }
                case WITHDRAW: {
                    Response withdraw = stub.withdraw(WithdrawRequest.newBuilder()
                            .setCurrency(action.getCurrency().name())
                            .setAmount(action.getAmount())
                            .setUserId(userId)
                            .build());
                    log.debug("user {} has made withdraw {} {} result {}", userId, action.getAmount(),
                            action.getCurrency(), withdraw.getMessage());
                    break;
                }
            }
        }
    }
}
