package ee.pawa.management;

import ee.pawa.action.Action;

import java.util.List;

public class Round {
    private final List<Action> actions;
    private int count;

    public Round(List<Action> actions) {
        this.actions = actions;
        count = 0;
    }

    public Action getAction() {
        if (count >= actions.size()) return null;
        return actions.get(count++);
    }



}
