package ee.pawa.management;

import ee.pawa.action.Action;
import ee.pawa.action.ActionType;
import ee.pawa.action.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Random;

public class RoundManagers {
    private static final Logger log = LoggerFactory.getLogger(RoundManagers.class);
    private Random random = new Random();

    public Round getRandomRound() {
        int i = random.nextInt(3);
        log.debug("Selected " + i);
        if (i == 0) {
            return getRoundA();
        } else if (i == 1) {
            return getRoundB();
        } else {
            return getRoundC();
        }
    }

    private Round getRoundA() {
        return new Round(new ArrayList<Action>() {{
            add(new Action(ActionType.DEPOSIT, "100", Currency.USD));
            add(new Action(ActionType.WITHDRAW, "200", Currency.USD));
            add(new Action(ActionType.BALANCE, null, null));
            add(new Action(ActionType.WITHDRAW, "100", Currency.USD));
            add(new Action(ActionType.BALANCE, null, null));
            add(new Action(ActionType.WITHDRAW, "100", Currency.USD));
        }});
    }

    private Round getRoundB() {
        return new Round(new ArrayList<Action>() {{
            add(new Action(ActionType.WITHDRAW, "100", Currency.GBP));
            add(new Action(ActionType.DEPOSIT, "300", Currency.GBP));
            add(new Action(ActionType.WITHDRAW, "100", Currency.GBP));
            add(new Action(ActionType.WITHDRAW, "100", Currency.GBP));
            add(new Action(ActionType.WITHDRAW, "100", Currency.GBP));
        }});
    }

    private Round getRoundC() {
        return new Round(new ArrayList<Action>() {{
            add(new Action(ActionType.BALANCE, null, null));
            add(new Action(ActionType.DEPOSIT, "100", Currency.USD));
            add(new Action(ActionType.DEPOSIT, "100", Currency.USD));
            add(new Action(ActionType.WITHDRAW, "100", Currency.USD));
            add(new Action(ActionType.DEPOSIT, "100", Currency.USD));
            add(new Action(ActionType.BALANCE, null, null));
            add(new Action(ActionType.WITHDRAW, "200", Currency.USD));
            add(new Action(ActionType.BALANCE, null, null));
        }});
    }
}
