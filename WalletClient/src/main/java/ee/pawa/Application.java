package ee.pawa;

import ee.pawa.grpc.WalletServiceGrpc;
import ee.pawa.management.Round;
import ee.pawa.management.RoundExecutor;
import ee.pawa.management.RoundManagers;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("wallet-server", 8080)
                .usePlaintext()
                .build();

        WalletServiceGrpc.WalletServiceBlockingStub stub
                = WalletServiceGrpc.newBlockingStub(channel);

        int usersCount = 1;
        int threadsPerUser = 1;
        int roundsPerThread = 1;
        int count = 0;
        while (count < args.length){
            switch (args[count]){
                case "-users": {
                    count++;
                    usersCount = getIntValue(args[count]);
                    break;
                }
                case "-concurrent_threads_per_user": {
                    count++;
                    threadsPerUser = getIntValue(args[count]);
                    break;
                }
                case "-rounds_per_thread": {
                    count++;
                    roundsPerThread = getIntValue(args[count]);
                    break;
                }
            }
            count++;
        }

        log.debug("users: {}", usersCount);
        log.debug("concurrent_threads_per_user: {}", threadsPerUser);
        log.debug("rounds_per_thread: {}", roundsPerThread);

        final int tpu = threadsPerUser;
        final int rpt = roundsPerThread;
        ExecutorService userExecutorService = Executors.newFixedThreadPool(usersCount);
        for (int i = 1; i <= usersCount; i++) {
            final long userId = i;
            userExecutorService.execute(() -> {
                ExecutorService userActionsExecutorService = Executors.newFixedThreadPool(tpu);
                for (int j = 0; j < tpu; j++) {
                    userActionsExecutorService.submit(() -> {
                        for (int k = 0; k < rpt; k++) {
                            Round randomRound = new RoundManagers().getRandomRound();
                            new RoundExecutor(stub, randomRound, userId).execute();
                        }
                    });
                }
                userActionsExecutorService.shutdown();
            });
        }
        userExecutorService.shutdown();
    }

    private static int getIntValue(String s) {
        if (s != null && s.length() > 0) return Integer.valueOf(s);
        return 1;
    }
}
