System requirements: Gradle, docker-compose.
Postgres script in postgres/init.sql
CLI parameters are set in docker-compose.yaml, at 25th line.

For running in Unix system it's necessary to perform command ./run.sh in the terminal.  In a Windows system, it's necessary to rename the file run.sh to run.bat and execute it.

Estimations:
Balance request: 536 rps.
Deposit request: 320 rps.
Withdraw request: 251 rps.
All these numbers were reached by concurrent Jmeter JUnit test, which was executed in 50 threads 2000 times.

Update for select is used to avoid "dirty reading" in withdrawing operations.
A transaction is used to avoid "lost update" in deposit operations.
