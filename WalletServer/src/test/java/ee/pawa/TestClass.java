package ee.pawa;

import ee.pawa.constant.Currency;
import ee.pawa.dao.impl.AccountDaoImpl;
import ee.pawa.dto.AccountDto;
import ee.pawa.utils.HibernateUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.yandex.qatools.embed.postgresql.EmbeddedPostgres;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static ru.yandex.qatools.embed.postgresql.distribution.Version.Main.V9_6;


public class TestClass {
    private EmbeddedPostgres postgres;
    public TestClass(){}

    @Before
    public void testEmbeddedPg() throws Exception {
        postgres = new EmbeddedPostgres(V9_6);
        final String url = postgres.start("localhost", 5431, "wallet", "postgres",
                "postgres");
        final Connection connection = DriverManager.getConnection(url);
        connection.createStatement().execute("DROP TABLE if exists public.account;");
        connection.createStatement().execute("DROP TABLE if exists public.\"user\";");
        connection.createStatement().execute("CREATE TABLE public.account (id bigint NOT NULL,\n" +
                "  amount bigint,\n" +
                "  currency character(3),\n" +
                "  user_id bigint,\n" +
                "  CONSTRAINT account_pkey PRIMARY KEY (id));");

        connection.createStatement().execute("CREATE TABLE public.\"user\" (id bigint NOT NULL,\n" +
                "  name text,\n" +
                "  CONSTRAINT user_pkey PRIMARY KEY (id));");
        connection.createStatement().execute("INSERT INTO public.account VALUES (1,0,'USD',1);");
        connection.createStatement().execute("INSERT INTO public.account VALUES (2,0,'EUR',1);");
        connection.createStatement().execute("INSERT INTO public.\"user\" VALUES (1,'user');");
        connection.close();
    }

    @Test
    public void testCase() {
        AccountDaoImpl accountDao = new AccountDaoImpl();
        try {
            accountDao.withdraw(1L, BigDecimal.valueOf(200), Currency.USD);
        } catch (Exception e) {
            assertEquals("insufficient_funds", e.getMessage());
        }
        try {
            accountDao.deposit(1L, BigDecimal.valueOf(100), Currency.USD);
        } catch (Exception e) {
            assert(false);
        }
        List<AccountDto> accountsByUser = accountDao.getAccountsByUser(1L);
        assertEquals(2, accountsByUser.size());
        AccountDto accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.USD).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.USD));
        assertEquals("100", accountDto.getAmount());
        assertEquals(Currency.USD, accountDto.getCurrency());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.EUR).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(100), Currency.EUR));

        assertEquals("0", accountDto.getAmount());
        assertEquals(Currency.EUR, accountDto.getCurrency());
        try {
            accountDao.withdraw(1L, BigDecimal.valueOf(200), Currency.USD);
        } catch (Exception e) {
            assertEquals("insufficient_funds", e.getMessage());
        }
        try {
            accountDao.deposit(1L, BigDecimal.valueOf(100), Currency.EUR);
        } catch (Exception e) {
            assert(false);
        }
        accountsByUser = accountDao.getAccountsByUser(1L);
        assertEquals(2, accountsByUser.size());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.USD).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.USD));
        assertEquals("100", accountDto.getAmount());
        assertEquals(Currency.USD, accountDto.getCurrency());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.EUR).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.EUR));
        assertEquals("100", accountDto.getAmount());
        assertEquals(Currency.EUR, accountDto.getCurrency());
        try {
            accountDao.withdraw(1L, BigDecimal.valueOf(200), Currency.USD);
        } catch (Exception e) {
            assertEquals("insufficient_funds", e.getMessage());
        }
        try {
            accountDao.deposit(1L, BigDecimal.valueOf(100), Currency.USD);
        } catch (Exception e) {
            assert(false);
        }

        accountsByUser = accountDao.getAccountsByUser(1L);
        assertEquals(2, accountsByUser.size());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.USD).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.USD));
        assertEquals("200", accountDto.getAmount());
        assertEquals(Currency.USD, accountDto.getCurrency());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.EUR).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.EUR));
        assertEquals("100", accountDto.getAmount());
        assertEquals(Currency.EUR, accountDto.getCurrency());
        try {
            accountDao.withdraw(1L, BigDecimal.valueOf(200), Currency.USD);
        } catch (Exception e) {
            assert(false);
        }

        accountsByUser = accountDao.getAccountsByUser(1L);
        assertEquals(2, accountsByUser.size());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.USD).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(100), Currency.USD));
        assertEquals("0", accountDto.getAmount());
        assertEquals(Currency.USD, accountDto.getCurrency());
        accountDto = accountsByUser.stream().filter(acc-> acc.getCurrency() == Currency.EUR).findAny()
                .orElse(new AccountDto(BigDecimal.valueOf(0), Currency.EUR));
        assertEquals("100", accountDto.getAmount());
        assertEquals(Currency.EUR, accountDto.getCurrency());
        try {
            accountDao.withdraw(1L, BigDecimal.valueOf(200), Currency.USD);
        } catch (Exception e) {
            assertEquals("insufficient_funds", e.getMessage());
        }
        assert(true);


    }

    @After
    public void cleanUp() {
        HibernateUtil.shutdown();
        postgres.stop();
    }

}
