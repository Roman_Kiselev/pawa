package ee.pawa.dto;

import ee.pawa.constant.Currency;

import java.math.BigDecimal;

public class AccountDto {
    private String amount;
    private Currency currency;

    public AccountDto() {
    }

    public AccountDto(BigDecimal amount, Currency currency) {
        this.amount = String.valueOf(amount);
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
