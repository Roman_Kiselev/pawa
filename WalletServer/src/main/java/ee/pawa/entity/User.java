package ee.pawa.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity(name = "user")
@Table(name = "user", schema = "public")
public class User implements Serializable {

    @Id
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Account> accounts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }
}
