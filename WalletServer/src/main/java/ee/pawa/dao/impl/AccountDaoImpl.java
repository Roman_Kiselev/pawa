package ee.pawa.dao.impl;

import ee.pawa.constant.Currency;
import ee.pawa.dao.AccountDao;
import ee.pawa.dto.AccountDto;
import ee.pawa.entity.Account;
import ee.pawa.utils.HibernateUtil;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class AccountDaoImpl implements AccountDao {
    private static final Logger log = LoggerFactory.getLogger(AccountDaoImpl.class);

    @Override
    public List<AccountDto> getAccountsByUser(Long userId) {
        Session session = getSessionWithoutTransaction();
        List<AccountDto> result = session.createQuery("select new ee.pawa.dto.AccountDto(a.amount, " +
                "a.currency) from account a where a.user.id = :id")
                .setParameter("id", userId)
                .setReadOnly(true)
                .list();
        session.close();
        return result;
    }

    @Override
    public void deposit(Long userId, BigDecimal amount, Currency currency) throws Exception {
        Session session = getSessionWithoutTransaction();
        Account properAccount = getAccountByUserIdAndCurrency(session, userId, currency);
        session.beginTransaction();
        session.createSQLQuery("update account set amount = amount + :amount where id=:id")
                .setParameter("amount", amount)
                .setParameter("id", properAccount.getId())
                .executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void withdraw(Long userId, BigDecimal amount, Currency currency) throws Exception {
        Session session = getSessionWithTransaction();
        if (session != null) {
            try {
                Account properAccount = getAccountByUserIdAndCurrencyForUpdate(session, userId, currency);
                if (properAccount.getAmount().compareTo(amount) < 0) throw new Exception("insufficient_funds");
                properAccount.setAmount(properAccount.getAmount().subtract(amount));
                session.update(properAccount);
            } finally {
                session.getTransaction().commit();
                session.close();
            }

        }
    }

    private Session getSessionWithTransaction() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            return session;
        } catch (Exception e) {
            log.error("error on session", e.getMessage(), e);
        }
        return null;
    }

    private Session getSessionWithoutTransaction() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    private Account getAccountByUserIdAndCurrency(Session session, Long userId, Currency currency) throws Exception {
        Optional<Account> account = Optional.ofNullable((Account) session.createQuery("select a " +
                "from account a where a.user.id = :id and a.currency = :currency")
                .setParameter("id", userId)
                .setParameter("currency", currency)
                .uniqueResult());
        return account.orElseThrow(() -> new Exception("Unknown currency"));
    }

    private Account getAccountByUserIdAndCurrencyForUpdate(Session session, Long userId, Currency currency) throws Exception {
        Optional<Account> account = Optional.ofNullable((Account) session.createQuery("select a " +
                "from account a where a.user.id = :id and a.currency = :currency")
                .setParameter("id", userId)
                .setParameter("currency", currency)
                .setLockOptions(LockOptions.UPGRADE)
                .uniqueResult());
        return account.orElseThrow(() -> new Exception("Unknown currency"));
    }
}
