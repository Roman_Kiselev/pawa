package ee.pawa.dao;

import ee.pawa.constant.Currency;
import ee.pawa.dto.AccountDto;

import java.math.BigDecimal;
import java.util.List;

public interface AccountDao {
    List<AccountDto> getAccountsByUser(Long userId);
    void deposit(Long userId, BigDecimal amount, Currency currency) throws Exception;
    void withdraw(Long userId, BigDecimal amount, Currency currency) throws Exception;
}
