package ee.pawa.service;

import ee.pawa.constant.Currency;
import ee.pawa.dao.AccountDao;
import ee.pawa.dto.AccountDto;
import ee.pawa.grpc.*;
import ee.pawa.grpc.Balance;
import ee.pawa.grpc.BalanceRequest;
import ee.pawa.grpc.BalanceResponse;
import ee.pawa.grpc.DepositRequest;
import ee.pawa.grpc.Response;
import ee.pawa.grpc.WithdrawRequest;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class WS extends WalletServiceGrpc.WalletServiceImplBase {

    private final AccountDao accountDao;
    private static final Logger log = LoggerFactory.getLogger(WS.class);

    public WS(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void deposit(DepositRequest request, StreamObserver<Response> responseObserver) {
        log.debug("deposit");
        String result = null;
        try {
            accountDao.deposit(request.getUserId(), new BigDecimal(request.getAmount()),
                    Currency.valueOf(request.getCurrency()));
        } catch (Exception e) {
            result = e.getMessage();
        }
        Response response = Response.newBuilder().setMessage(result == null ? "ok" : result).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void withdraw(WithdrawRequest request, StreamObserver<Response> responseObserver) {
        log.debug("withdraw");
        String result = null;
        try {
            accountDao.withdraw(request.getUserId(), new BigDecimal(request.getAmount()),
                    Currency.valueOf(request.getCurrency()));
        } catch (Exception e) {
            result = e.getMessage();
        }
        Response response = Response.newBuilder().setMessage(result == null ? "ok" : result).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void balance(BalanceRequest request, StreamObserver<BalanceResponse> responseObserver) {
        log.debug("balance");
        List<AccountDto> accountsByUser = accountDao.getAccountsByUser(request.getUserId());
        List<Balance> collect = accountsByUser.stream().map(account -> Balance.newBuilder()
                .setAmount(account.getAmount())
                .setCurrency(account.getCurrency().name()).build())
                .collect(Collectors.toList());
        responseObserver.onNext(BalanceResponse.newBuilder().addAllBalance(collect).build());
        responseObserver.onCompleted();
    }
}
