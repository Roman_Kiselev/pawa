package ee.pawa.constant;

public enum Currency {
    EUR, USD, GBP
}
