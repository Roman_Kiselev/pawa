package ee;

import ee.pawa.dao.impl.AccountDaoImpl;
import ee.pawa.service.WS;
import ee.pawa.utils.HibernateUtil;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        log.debug("begin");
        HibernateUtil.getSessionFactory();
        Server server = ServerBuilder
                .forPort(8080)
                .addService(new WS(new AccountDaoImpl())).build();

        server.start();
        server.awaitTermination();
        HibernateUtil.shutdown();
    }
}
